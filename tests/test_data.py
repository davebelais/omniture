from datetime import datetime, timezone, timedelta, date

from omniture.data import str2date, str2datetime


def test_str2datetime():
    assert str2datetime('2016-11-03T09:10:10-0700') == datetime(
        2016, 11, 3, 9, 10, 10,
        tzinfo=timezone(timedelta(-1, 61200))
    )
    assert str2datetime('2016-11-03 09:10:10-0700') == datetime(
        2016, 11, 3, 9, 10, 10,
        tzinfo=timezone(timedelta(-1, 61200))
    )
    assert str2datetime('2016-11-03T09:10:10UTC-0700') == datetime(
        2016, 11, 3, 9, 10, 10,
        tzinfo=timezone(timedelta(-1, 61200))
    )
    assert str2datetime('2016-11-03 09:10:10') == datetime(
        2016, 11, 3, 9, 10, 10
    )
    assert str2datetime('2016-11-03 09:10:10UTC-0700') == datetime(
        2016, 11, 3, 9, 10, 10,
        tzinfo=timezone(timedelta(-1, 61200))
    )


def test_str2date():
    assert str2date('2016-11-03') == date(
        2016, 11, 3
    )


if __name__ == '__main__':
    test_str2datetime()
    test_str2date()
