# omniture

A python package for interfacing with the Omniture Web Services' API
(https://marketing.adobe.com/developer/documentation).

To install:

```commandline
$ pip install git+https://github.com/davebelais/omniture.git
```
